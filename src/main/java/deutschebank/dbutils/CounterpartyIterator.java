package deutschebank.dbutils;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CounterpartyIterator {
   ResultSet rowIterator;

   CounterpartyIterator( ResultSet rs )
   {
      rowIterator = rs;
   }

   public boolean  first() throws SQLException
   {
      return rowIterator.first();
   }

   public boolean last() throws SQLException
   {
      return rowIterator.last();
   }
   public boolean next() throws SQLException
   {
      return rowIterator.next();
   }

   public boolean prior() throws SQLException
   {
      return rowIterator.previous();
   }

   public   String  getCounterpartyName() throws SQLException
   {
      return rowIterator.getString("counterparty_name");
   }

   public   String  getCounterpartyStatus() throws SQLException{

      return rowIterator.getString("counterparty_status");
   }

   public String getCounterpartyDate() throws SQLException {
      return rowIterator.getString("counterparty_date_registered");
   }

   Counterparty   buildCounterparty() throws SQLException
   {
      Counterparty result = new Counterparty( getCounterpartyName(), getCounterpartyStatus(), getCounterpartyDate() );

      return result;
   }
}
