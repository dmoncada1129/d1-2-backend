package deutschebank.dbutils;

import com.fasterxml.jackson.databind.ObjectMapper;
import deutschebank.MainUnit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InstrumentController {

    public String getInstruments() {
        try {
            DBConnector connector = DBConnector.getConnector();

            PropertyLoader pLoader = PropertyLoader.getLoader();

            Properties pp = pLoader.getPropValues("dbConnector.properties");

            connector.connect( pp );

            // connect to Instrument table
            ObjectMapper mapper = new ObjectMapper();
            InstrumentHandler theInstrumentHandler = InstrumentHandler.getLoader();
//            Deal theDeal = theDealHandler.loadFromDB(connector.getConnection(), "selvyn", "gradprog2016");
            ArrayList<Instrument> theInstruments = theInstrumentHandler.loadFromDB(connector.getConnection());

            Instrument[] InstrumentArray = new Instrument[theInstruments.size()];
            theInstruments.toArray(InstrumentArray);
            theInstruments.forEach((instrument) ->
                    {
                        System.out.println(instrument.getInstrumentID() + "**" + instrument.getInstrumentName());
                    }
            );

            // Convert object to JSON string
            String jsonInString = mapper.writeValueAsString(theInstruments);
//            System.out.println(jsonInString);

            // Convert object to JSON string and pretty print
            jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(theInstruments);
//            System.out.println(jsonInString);
//            return jsonInString;
            return jsonInString;
        } catch (IOException ex) {
            Logger.getLogger(MainUnit.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }
}
