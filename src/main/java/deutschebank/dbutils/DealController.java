package deutschebank.dbutils;

import com.fasterxml.jackson.databind.ObjectMapper;
import deutschebank.MainUnit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DealController {

    public String getDeals() {
        try {
            DBConnector connector = DBConnector.getConnector();

            PropertyLoader pLoader = PropertyLoader.getLoader();

            Properties pp = pLoader.getPropValues("dbConnector.properties");

            connector.connect( pp );

            // connect to Deal table
            ObjectMapper mapper = new ObjectMapper();
            DealHandler theDealHandler = DealHandler.getLoader();
//            Deal theDeal = theDealHandler.loadFromDB(connector.getConnection(), "selvyn", "gradprog2016");
            ArrayList<Deal> theDeals = theDealHandler.loadFromDB(connector.getConnection());

            Deal[] dealArray = new Deal[theDeals.size()];
            theDeals.toArray(dealArray);
//            theDeals.forEach((deal) ->
//                    {
//                        System.out.println(deal.getDealID() + "//" + deal.getDealTime());
//                    }
//            );

            // Convert object to JSON string
            String jsonInString = mapper.writeValueAsString(theDeals);
//            System.out.println(jsonInString);

            // Convert object to JSON string and pretty print
            jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(theDeals);
//            System.out.println(jsonInString);
//            return jsonInString;
            return jsonInString;
        } catch (IOException ex) {
            Logger.getLogger(MainUnit.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }
}
