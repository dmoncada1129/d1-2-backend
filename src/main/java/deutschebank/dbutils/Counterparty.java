package deutschebank.dbutils;

import java.sql.Date;

public class Counterparty {
    private String itsCounterpartyName;
    private String itsCounterpartyStatus;
    private String itsCounterpartyDate;

    public Counterparty(String name, String status, String date) {
        itsCounterpartyName = name;
        itsCounterpartyStatus = status;
        itsCounterpartyDate = date;
    }

    public String getCounterpartyName() {
        return itsCounterpartyName;
    }

    public String getCounterpartyStatus() {
        return itsCounterpartyStatus;
    }

    public String getCounterpartyDate() {
        return itsCounterpartyDate;
    }

    public void setCounterpartyName(String itsCounterpartyName) {
        this.itsCounterpartyName = itsCounterpartyName ;
    }

    public void setCounterpartyStatus(String itsCounterpartyStatus) {
        this.itsCounterpartyStatus = itsCounterpartyStatus;
    }

    public void setCounterpartyDate(String itsCounterpartyDate) {
        this.itsCounterpartyDate = itsCounterpartyDate;
    }
}
