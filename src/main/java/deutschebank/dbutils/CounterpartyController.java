package deutschebank.dbutils;

import com.fasterxml.jackson.databind.ObjectMapper;
import deutschebank.MainUnit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CounterpartyController {

    public String getCounterparties() {
        try {
            DBConnector connector = DBConnector.getConnector();

            PropertyLoader pLoader = PropertyLoader.getLoader();

            Properties pp = pLoader.getPropValues("dbConnector.properties");

            connector.connect( pp );

            // connect to Counterparty table
            ObjectMapper mapper = new ObjectMapper();
            CounterpartyHandler theCounterpartyHandler = CounterpartyHandler.getLoader();
//            Deal theDeal = theDealHandler.loadFromDB(connector.getConnection(), "selvyn", "gradprog2016");
            ArrayList<Counterparty> theCounterparties = theCounterpartyHandler.loadFromDB(connector.getConnection());

            Counterparty[] counterpartyArray = new Counterparty[theCounterparties.size()];
            theCounterparties.toArray(counterpartyArray);
//            theDeals.forEach((deal) ->
//                    {
//                        System.out.println(deal.getDealID() + "//" + deal.getDealTime());
//                    }
//            );

            // Convert object to JSON string
            String jsonInString = mapper.writeValueAsString(theCounterparties);
//            System.out.println(jsonInString);

            // Convert object to JSON string and pretty print
            jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(theCounterparties);
//            System.out.println(jsonInString);
//            return jsonInString;
            return jsonInString;
        } catch (IOException ex) {
            Logger.getLogger(MainUnit.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }
}
